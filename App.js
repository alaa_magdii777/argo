import React, { useEffect, useMemo, useState } from 'react';
import { SafeAreaProvider } from "react-native-safe-area-context";
import MainNavigation from "./src/navigations/MainNavigation";
import 'react-native-gesture-handler';
import AuthContext from "./src/contexts/AuthContext"; 

// export default class App extends Component {
const App = () => { 
  const [authData, setAuthData] = useState({});
  const authContextValue = useMemo(() => ({ authData, setAuthData }), [authData]);

  // render() {
  return (
    <AuthContext.Provider value={authContextValue}>
      <SafeAreaProvider>
        <MainNavigation />
      </SafeAreaProvider>
    </AuthContext.Provider>

  )
  // }
}

export default App;




