const colorsTheme = {
  dark: false,
  colors: {  
    primary: '#1E90FF', 
    purpleLight:'#723cfa',
    gray:'#8F8E94',
    textBlack:"#373737",
    white:"#FFFFFF",
  },
};

export default colorsTheme
