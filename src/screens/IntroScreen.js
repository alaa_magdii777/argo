import React, { Component } from 'react'
import { Text, View,ScrollView} from 'react-native'
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import colorsTheme from "../utils/colorsTheme";

const { colors } = colorsTheme

export default class IntroScreen extends Component {
    constructor(props){
        super(props);   
    } 

    componentDidMount(){
        // Start counting when the page is loaded
        this.timeoutHandle = setTimeout(()=>{
            // Add your logic for the transition
            this.props.navigation.navigate('login') // what to push here?
        }, 5000);
    }
    componentWillUnmount(){
        clearTimeout(this.timeoutHandle); 
    }
    render() {
        let {navigation}=this.props;
        return (
            <ScrollView style={{backgroundColor:colors.primary}}>
            <View style={{flex:1,alignItems:"center",marginTop:40,}}>
                <Text style={{fontSize:40,paddingVertical:30}}>It Share </Text>
                {/* <Button onPress={() => navigation.navigate('login')}
                    title="go to login"
                    containerStyle={{alignContent:"center"}}
                    /> */}
            </View>
            </ScrollView>
        )
    }
}
