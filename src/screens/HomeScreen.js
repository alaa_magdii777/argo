import React, { Component } from 'react';
import { Text, View,ScrollView,TouchableOpacity,Image} from 'react-native';
import Font from "../utils/fonts";
import colorsTheme from "../utils/colorsTheme";
import Images from "../Images";
import Header from "../components/Header";

const {Family}=Font;
const {Size}=Font;
const {colors}=colorsTheme


export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { navigation } = this.props;
        return (
            <ScrollView>
             <Header navigation={navigation} />
            <View style={{flex:1,alignItems:"center",marginTop:40}}>
                <Text style={{color:colors.primary,fontSize:Font.Size.fs_19,paddingVertical:10,fontFamily:Font.Family.semi_bold,paddingVertical:20}}>Welcome in Home</Text>
                <Text style={{fontSize:20,paddingVertical:10,}}>Welcome in Home</Text>
            </View>
            </ScrollView>
        )
    }
}




