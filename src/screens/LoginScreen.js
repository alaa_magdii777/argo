import React, { useState, useContext } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView, ImageBackground, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import AuthContext from "../contexts/AuthContext";
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import Images from "../Images";
import Font from "../utils/fonts";
import colorsTheme from "../utils/colorsTheme";
import { Formik } from 'formik';
import * as yup from 'yup';


const { Family } = Font;
const { Size } = Font;
const { colors } = colorsTheme

const validationSchema = yup.object().shape({
    email: yup.string().required().email().label("Email"),
    password: yup.string().required().label("Password")
        .min(2, "it seems a short...")
        .max(10, "try shorter password - max 10 character"),
})

const data = {
    "email": "sagaahmed@gmail.com",
    "password": "123456789",
}

const LoginScreen = ({ navigation }) => {
    const { authData, setAuthData } = useContext(AuthContext);

    //alert(JSON.stringify(auth))

    const Login = () => {
        axios.post(`https://itshareback.herokuapp.com/studentLogIn`, data, {
            // headers:
            //     [
            //         {
            //             "key": "Content-Type",
            //             "name": "Content-Type",
            //             "value": "application/json",
            //             "type": "text",
            //         }
            //     ],


        })
            .then(response => {
                setAuthData(response.data)
                alert(JSON.stringify(response.data))
                console.log(response.data);
                navigation.navigate('home'),
                    AsyncStorage.setItem('@currentUser', JSON.stringify(response.data));


            })
            .catch(error => {
                alert(error.message)
            })
    }

    return (
        <ScrollView>
            <View style={{ flex: 1, alignItems: "center", marginTop: 40 }}>

                {/* <ImageBackground
                    style={{ height: 63, width: 220, alignItems: "center" }}
                    source={require('../assets/images/logo.png')}
                /> */}
                <Text style={{color:colors.primary,fontSize:30,fontFamily:Font.Family.semi_bold}}>It Share</Text>



                <View style={styles.up_form}>
                    <View style={styles.form}>
                        <View style={styles.form_up_inputs}>
                            <Formik
                                initialValues={{ email: '', password: '', checkbox: '' }}
                                onSubmit={(actions) => {
                                    // alert(JSON.stringify(values));
                                    Login()

                                        .then(() => {
                                            //save-in-DataBase
                                        })

                                    setTimeout(() => {
                                        actions.setSubmitting(false)
                                    }, 1000)

                                        .catch((error) => {
                                            actions.setFieldError("general", error.message);
                                        })
                                        .finally(() => {
                                            actions.setSubmitting(false);
                                        });
                                }}
                                validationSchema={validationSchema}
                            >

                                {formikProps => (
                                    <React.Fragment>

                                        <View style={styles.up_input}>
                                            <Image style={styles.input_icon} source={Images.email} />
                                            <TextInput
                                                maxLength={50}
                                                style={styles.input}
                                                type="email"
                                                placeholder='Email'
                                                label="Email"
                                                formikKey="email"
                                                autoFocus
                                                onChangeText={formikProps.handleChange('email')} />
                                        </View>

                                        <Text style={{ color: "red", flexDirection: "row", paddingVertical: 5 }}>{formikProps.errors.name}</Text>


                                        <View style={styles.up_input}>
                                            <Image style={styles.input_icon} source={Images.password} />
                                            <TextInput
                                                maxLength={50}
                                                style={styles.input}
                                                type="password"
                                                placeholder='Password'
                                                label="Password"
                                                formikKey="password"
                                                autoFocus
                                                secureTextEntry
                                                onChangeText={formikProps.handleChange('password')} />
                                        </View>
                                        <Text style={{ color: "red", flexDirection: "row", paddingVertical: 5 }}>{formikProps.errors.password}</Text>


                                        {formikProps.isSubmitting ?
                                            (
                                                <React.Fragment>
                                                    <TouchableOpacity onPress={formikProps.handleSubmit}

                                                        activeOpacity={.9}
                                                        style={styles.button}>
                                                        <Text style={styles.buttonText}>Login</Text>
                                                    </TouchableOpacity>
                                                    <Text style={{ color: "red" }}>
                                                        {formikProps.errors.general}
                                                    </Text>
                                                </React.Fragment>
                                            )
                                            : (
                                                <React.Fragment>
                                                    <TouchableOpacity onPress={formikProps.handleSubmit}

                                                        activeOpacity={.9}
                                                        style={styles.button}>
                                                        <Text style={styles.buttonText}>Login</Text>
                                                    </TouchableOpacity>
                                                    <Text style={{ color: "red" }}>
                                                        {formikProps.errors.general}
                                                    </Text>
                                                </React.Fragment>
                                            )}
                                    </React.Fragment>
                                )}
                            </Formik>


                        </View>
                    </View>
                </View>

            </View>
        </ScrollView>
    )
}


LoginScreen.defaultProps = {};


const styles = StyleSheet.create({


    up_form: {
        width: '100%',
        elevation: 20,
        maxWidth: 400,
        marginBottom: 100,
    },
    form: {
        flex: 1,
        // backgroundColor : '#fff',
        borderRadius: 15,
        borderWidth: 5,
        borderColor: '#d7d7d7',
        height: 320,
        padding: 20,
        paddingHorizontal: 0,
        margin: 20,
        marginTop: 40,
    },

    form_up_inputs: {
        paddingHorizontal: 20,
    },
    title: {
        textAlign: 'center',
        color: '#101010',
        fontSize: 16,
        lineHeight: 22,
        fontFamily: Font.Family.regular,
    },
    up_input: {
        height: 50,
        maxHeight: 50,
        overflow: 'hidden',
        borderRadius: 5,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#DCDCDC',
        marginTop: 10,
        backgroundColor: '#fff',
    },
    input: {
        flex: 1,
        lineHeight: 35,
        paddingVertical: 0,
        fontFamily: Font.Family.regular,
    },
    input_icon: {
        height: 22,
        width: 22,
        alignSelf: 'center',
        marginHorizontal: 5,
        resizeMode: 'contain'
    },

    button: {
        flex: 1,
        height: 35,
        paddingHorizontal: 20,
        borderRadius: 6,
        borderColor: 'blue',
        borderWidth: 1,
        backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        paddingVertical: 25,
    },

    buttonText: {
        color: '#fff',
        fontFamily: Font.Family.semi_bold,
        fontSize: 18
    },

})


export default LoginScreen;