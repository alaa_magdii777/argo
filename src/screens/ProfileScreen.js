import React, { Component } from 'react'
import { Text, View } from 'react-native'
import Header from "../components/Header";

export default class ProfileScreen extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let { navigation } = this.props;
        return (
            <View>
                <Header navigation={navigation} />
                <Text> ProfileScreen </Text>
            </View>
        )
    }
}
