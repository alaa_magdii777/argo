import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from "../../screens/HomeScreen"
import ProfileScreen from "../../screens/ProfileScreen"
import DrawerContent from "./DrawerContent";

const Drawer = createDrawerNavigator();

function drawerNav() {
  // const drawerNav =({}) => {

  return (

    //  {/*   <Drawer.Navigator  drawerContent={props => <DrawerContent {...props} />}  */}

    <Drawer.Navigator

      drawerContentOptions={{
        activeTintColor: 'white',
        activeBackgroundColor: "#4B0082",
        inactiveTintColor: "black",
        activeBackgroundColor: "#4B0082",
        backgroundColor: "white",
        itemStyle: { marginVertical: 5 },
      }}
    >
      <Drawer.Screen
        name="Home"
        options={{ drawerLabel: 'Home' }}
        component={HomeScreen} />
      <Drawer.Screen
        name="Profile"
        options={{ drawerLabel: 'Profile' }}
        component={ProfileScreen} />
    </Drawer.Navigator>
  );
}

export default drawerNav;