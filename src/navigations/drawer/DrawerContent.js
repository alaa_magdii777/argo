import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {
    createDrawerNavigator,
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';
import Images from "../../Images";


export function DrawerContent(props) {

    const {navigation} = props;
    return (
        <View style={{flex:1}}>

            <DrawerContentScrollView {...props} contentContainerStyle={{minHeight:'100%'}}>

                <View style={styles.section_logo}>
                    <Image style={styles.logo} source={Images.drawer}/>
                </View>

                <View style={styles.section_user}>
                    {/* <Image style={styles.user} source={Images.holder}/> */}
                    <Text style={styles.user_name}>user name</Text>
                </View>
 
                <CustomDrawerItem navigation={navigation} icon = {Images.logout} label="home" screenName = "HomeScreen" />
                <CustomDrawerItem navigation={navigation} icon = {Images.logout} label="profile"   screenName = "ProfileScreen" />
              
                <View style={styles.line} />

               

                <View style={{height : 100}} />


                <CustomDrawerItem navigation={navigation} icon = {Images.logout} label="logout" screenName = "login" />

                {/* <View style={CommonStyles.centerContent}>
                    <TouchableOpacity  onPress={() => navigation.navigate('login')}
                        activeOpacity={.9}
                        style={styles.button}
                        onPress={null}>
                        <Image style={styles.button_image} resizeMode="contain" source={Images.side_menu_logout}/>
                        <Text style={styles.buttonText}>{t('logout')}</Text>
                    </TouchableOpacity>
                </View> */}


            </DrawerContentScrollView>
            </View>
);
}




const CustomDrawerItem = ({label, screenName, icon, navigation}) => {
    return (
        <TouchableOpacity activeOpacity={.9} style={styles.item} onPress={() => navigation.navigate(screenName)}>
            {/* <Image style={styles.item_image} resizeMode="contain" source={icon}/> */}
            <Text style={styles.itemText}>opeeeen</Text>
        </TouchableOpacity>
    )
};




const styles = StyleSheet.create({

    section_logo : {
        // ...CommonStyles.centerContent,
        paddingTop : 60,
    },
    logo : {
        width : 140,
        height : 55,
    },
    section_user : {
        // ...CommonStyles.centerContent,
        paddingTop : 30,
    },
    user : {
        width : 100,
        height : 100,
        borderRadius : 100,
        borderWidth : 5,
        borderColor : '#F9F9F999',
    },
    user_name : {
        textAlign: 'center',
        paddingVertical : 10,
        marginBottom : 20,
        fontSize: 16,
        // fontFamily : Constants.fontRegular,
    },
    item : {
        flexDirection : 'row',
        padding : 10,
    },
    itemText : {
        marginHorizontal : 8,
        // fontFamily : Constants.fontRegular,
    },
    item_image : {
        width : 20,
        height : 20,
        marginHorizontal : 8,
    },
    line : {
        margin : 20,
        height : 1,
        backgroundColor : 'rgba(255, 255,241, .7)',
    },
    button : {
        marginBottom : 30,
        width : 120,
        padding : 5,
        paddingHorizontal : 20,
        borderRadius : 6,
        borderColor : '#1010103F',
        borderWidth : 1,
        backgroundColor : '#AF0FA4',
        justifyContent: 'center',
        flexDirection : 'row',
    },
    buttonText : {
        color : '#fff',
        fontSize : 12,
        // fontFamily : Constants.fontRegular,
    },

    button_image : {
        width : 16,
        height : 16,
        marginHorizontal : 6,
    },



});



